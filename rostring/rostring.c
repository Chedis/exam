/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rostring.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pacadis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 10:08:13 by pacadis           #+#    #+#             */
/*   Updated: 2017/01/28 11:20:45 by pacadis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	ft_putstr(char *str)
{
	int 	i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void	remove_spaces(char	*str)
{
	int 	i;
	int		j;

	i = 0;
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == 32)
	{
		j = i;
		while (str[j])
		{
			str[j] = str[j + 1];
			j++;
		}
	}
	while (str[i])
	{
		if ((str[i] >= 9 && str[i] <= 13) || str[i] == 32)
			if ((str[i + 1] >= 9 && str[i + 1] <= 13) || str[i + 1] == 32 ||
					str[i + 1] == '\0')
			{
				j = i;
				while (str[j])
				{
					str[j] = str[j + 1];
					j++;
				}
				i--;
			}
		i++;
	}
}

void	create_str(char *str)
{
	char	new_str[10000];
	char	fword[10000];
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (((str[i] < 9 || str[i] > 13) && str[i] != 32) && str[i])
	{
		fword[j] = str[i];
		i++;
		j++;
	}
	fword[j] = '\0';
	j = 0;
	while (str[i])
	{
		new_str[j] = str[i];
		i++;
		j++;
	}
	new_str[j] = 32;
	j++;
	i = 0;
	while (fword[i])
	{
		new_str[j] = fword[i];
		i++;
		j++;
	}
	new_str[j] = '\0';
	remove_spaces(new_str);
	ft_putstr(new_str);
}

int		main(int ac, char **av)
{
	char *str;

	if (ac < 2)
		write(1, "\n", 1);
	else
	{
		str = av[1];
		remove_spaces(str);
		create_str(str);
		ft_putchar('\n');
	}
	return (0);
}
