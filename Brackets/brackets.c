/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   brackets.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pacadis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 15:53:35 by pacadis           #+#    #+#             */
/*   Updated: 2017/01/21 15:55:06 by pacadis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int validArg(char *s){
	char a[1000];
	int vf = -1,i = 0;
	while (s[i] != '\0')
	{
		if (s[i] == '(' || s[i] == '[' || s[i] == '{')
			a[++vf] = s[i];
		else{
			if (s[i] == ')'){
				if ('(' == a[vf])
					vf--;
				else
					return (0);
			}
			else if (s[i] == ']'){
				if ('[' == a[vf])
					vf--;
				else
					return (0);
			}
			else if (s[i] == '}'){
				if ('{' == a[vf])
					vf--;
				else
					return (0);
			}
		}
		i++;
	}
	if (vf >=  0)
		return (0);
	return (1);
}

void ft_putstr(char *s){
	int i = 0;
	while (s[i] != '\0'){
		write(1,&s[i],1);
		i++;
	}
}

int main(int argc,char **argv){
	int i = 1;
	while (i < argc){
		if (validArg(argv[i]) == 0)
			ft_putstr("Error\n");
		else
			ft_putstr("OK\n");
		i++;
	}
	if (argc == 1)
		write(1,"\n",1);
	return (0);
}

