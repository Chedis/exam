/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_prime_sum.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pacadis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/14 12:21:49 by pacadis           #+#    #+#             */
/*   Updated: 2017/01/19 11:24:42 by pacadis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_atoi(char *str)
{
	int nbr;
	int i;
	int neg;

	i = 0;
	neg = 0;
	nbr = 0;
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == 32)
		i++;
	if (str[i] == '+' || str[i] == '-')
	{
		i++;
		if (!(str[i] >= 48 && str[i] <= 57))
			return (0);
	}
	if (str[i - 1] == '-')
		neg = 1;
	while (str[i] >= 48 && str[i] <= 57)
	{
		nbr = (nbr * 10) + (str[i] - 48);
		i++;
	}	
	if (neg == 1)
		nbr = nbr * (-1);
	return (nbr);
}

int		prim(int nr)
{
	int d;

	d = 2; 
	while (d <= nr / 2)
	{
		if (nr % d == 0)
			return (0);
		d++;
	}
	return (1);
}

int		check(char *str)
{
	int i;

	i = 0;
	if (str[0] == '-')
		return (0);
	while (str[i])
	{
		if (str[i] < '0' || str[i] > '9')
			return (0);
		i--;
	}
	return (1);
}

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	ft_putnbr(int nr)
{
	int temp;
	int size;

	size = 1;
	temp = nr;
	while ((temp /= 10) > 0)
		size *= 10;
	temp = nr;
	while (size)
	{
		ft_putchar((char)((temp / size)) + 48);
		temp %= size;
		size /= 10;
	}
}

void	ft_putnbr2(int nb)
{
	int aux;

	aux = nb;
	if (aux > 9)
	{
		ft_putnbr2(aux / 10);
		ft_putnbr2(aux % 10);
	}
	else
		ft_putchar(aux + '0');
}

int		main(int argc, char **argv)
{
	int nr;
	int sum;
	int d;

	d = 2;
	if (argc != 2 || ft_atoi(argv[1]) <= 1)
		write(1, "0\n", 2);
	else
	{
		sum = 0;
		nr = ft_atoi(argv[1]);
		while (d <= nr)
		{
			if (prim(d) == 1)
				sum = sum + d;
			d++;
		}
		ft_putnbr2(sum);
		write(1, "\n", 1);
	}
	return (0);
}
