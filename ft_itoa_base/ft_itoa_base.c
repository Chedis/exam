/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astanciu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/10 16:22:04 by astanciu          #+#    #+#             */
/*   Updated: 2017/01/11 17:03:39 by astanciu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

char	*ft_itoa_base(int n, int base)
{
	char	*str;
	char	*bases = "0123456789ABCDEF";
	int 	neg;
	long	num;
	int 	i;

	if (n == 0)
		return ("0");
	neg = 0;
	num = n;
	if (num < 0)
	{
		num = -num;
		if (base == 10)
			neg = 1;
	}
	str = (char*)malloc(sizeof(char) * 100);
	str[99] = '\0';
	i = 98;
	while (num)
	{
		str[i] = bases[num % base];
		num /= base;
		i--;
	}
	if (neg)
	{
		str[i] = '-';
		i--;
	}
	return (str + i + 1);
}

int 	main(int argc, char **argv)
{
	char *da;
	da = ft_itoa_base(atoi(argv[1]), atoi(argv[2]));
	printf("%s\n", da);
	return 0;
}
