/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rpn_calc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pacadis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 11:26:22 by pacadis           #+#    #+#             */
/*   Updated: 2017/02/11 11:26:17 by pacadis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		operands(char c, int a, int b)
{
	if (c == '+')
		return (a + b);
	else if (c == '-')
		return (a - b);
	else if (c == '*')
		return (a * b);
	else if (c == '/')
		return (a / b);
	return (a % b);	
}

int		is_digit(char c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int		is_sign(char c)
{
	if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%')
		return (1);
	return (0);
}

int		valid_string(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (!(is_digit(str[i])))
		{
			if (!(is_sign(str[i])))
			{
				if (str[i] != ' ')
					return (0);
			}
		}		
		i++;
	}
	return (1);
}

void	calculate(char *str)
{
	char	sign[100];
	char	aux[50];
	int		num[100];
	int		nr_sign;
	int		nr_digits;
	int		i;
	int		j;
	
	i = 0;
	nr_sign = 0;
	nr_digits = 0;
	while (str[i])
	{
		if (is_sign(str[i]))
		{
			sign[nr_sign] = str[i];
			nr_sign++;
			i++;
		}
		else
			if (is_digit(str[i]))
			{
				j = 0;
				while (is_digit(str[i]))
				{
					aux[j] = str[i];
					i++;
					j++;					
				}
				aux[j] = '\0';
				num[nr_digits] = atoi(aux);
				nr_digits++;
			}
			else
				i++;
	}
	if (nr_digits != nr_sign + 1)
		printf("%s\n", "Error");
	else
	{
		i = 0;
		j = 1;
		while (sign[i] && j < nr_digits)
		{
			if (num[j] == 0 || num[j - 1] == 0)
			{
				if (sign[i] == '%' || sign[i] == '/')
				{
					printf("%s\n", "Error");
					i = nr_sign + 1;
				}
				else
				{
					num[j] = operands(sign[i], num[j - 1], num[j]);
					j++;
					i++;
				}
			}
			else
			{
				num[j] = operands(sign[i], num[j - 1], num[j]);
				i++;
				j++;
			}
		}
		printf("%d\n", num[j - 1]);
	}
}

int		main(int ac, char **av)
{
	//printf("%s\n", av[1]);
	if (ac == 2)
	{
		if (valid_string(av[1]))
		{
			calculate(av[1]);
		}
		else
			printf("%s\n", "Error");
	}
	else
		printf("%s\n", "Error");
	return (0);
}
