/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_foreach_if.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pacadis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 14:42:42 by pacadis           #+#    #+#             */
/*   Updated: 2017/02/01 14:57:11 by pacadis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_list_foreach_if(t_list *begin_list, void	(*f)(void *), void *data_ref, int (*cmp)())
{
	t_list	*lista;
	
	lista = begin_list;
	while (lista->data != 0)
	{
		if ((*cmp)(lista->data, data_ref) == 0)
			(*f)(lista->data);
		lista = lista->next;
	}
}	
