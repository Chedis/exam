/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_list.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pacadis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 14:58:04 by pacadis           #+#    #+#             */
/*   Updated: 2017/02/01 14:59:34 by pacadis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	swap(t_list *a, t_list *b)
{
	int		data;

	data = a->data;
	a->data = b->data;
	b->data = data;
}

t_list	*sort_list(t_list *lst, int (*cmp)(int, int))
{
	t_list	*i;
	t_list	*j;

	if (!lst->next)
		return (lst);
	if (!lst->next->next)
	{
		if (!cmp(lst->data, lst->next->data))
			swap(lst, lst->next);
		return (lst);
	}
	i = lst;
	while (i->next)
	{
		j = i->next;
		while (j)
		{
			if (!cmp(i->data, j->data))
				swap(i, j);
			j = j->next;
		}
		i = i->next;
	}
	return (lst);
}
