/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pacadis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 14:52:07 by pacadis           #+#    #+#             */
/*   Updated: 2017/01/21 15:05:36 by pacadis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_itoa(int n)
{
	char	*str;
	int 	neg;
	long	num;
	int 	i;

	if (n == 0)
		return ("0");
	neg = 0;
	num = n;
	if (num < 0)
	{
		num = -num;
		neg = 1;
	}
	str = (char*)malloc(sizeof(char) * 100);
	str[99] = '\0';
	i = 98;
	while (num)
	{
		str[i] = num % 10 + '0';
		num /= 10;
		i--;
	}
	if (neg)
	{
		str[i] = '-';
		i--;
	}
	return (str + i + 1);
}
