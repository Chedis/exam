/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprime.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pacadis <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 11:50:55 by pacadis           #+#    #+#             */
/*   Updated: 2017/01/21 12:10:20 by pacadis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		check(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] < '0' || str[i] > '9')
			return (0);
		i++;
	}
	return (1);
}

int		main(int argc, char **argv)
{
	int d;
	int aux;

	d = 2;
	if (argc != 2)
		printf("\n");
	else
	{
		aux = atoi(argv[1]);
		if (check(argv[1]) == 1 && aux >= 1)
		{
			if (aux == 1)
				printf("%d\n", aux);
			else
			{
				while (d <= aux)
				{
					if (aux % d == 0)
					{
						printf("%d", d);
						if (d < aux)
							printf("%c", '*');
						aux /= d;
					}
					else
						d++;
				}
			printf("\n");
			}
		}
		else
			printf("\n");
	}
	return (0);
}
